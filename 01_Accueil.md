---
title: Avant Propos
author: Représentants étudiants du CRIUGM
date: Années 2015-2016
---

## Avant propos

**Bienvenue au centre de recherche** de l’Institut Universitaire de Gériatrie de Montréal (CRIUGM) !
En tant que représentants étudiants du centre, nous sommes très heureux de vous accueillir, virtuellement dans un premier temps à travers ce guide.

Ce guide a été rédigé dans l’objectif de fournir une **ressource synthétique d’informations** nécessaires aux nouveaux venus au centre. 
Le guide est à **destination des étudiants** affiliés au CRIUGM, qu’ils soient en maîtrise, doctorat, ou postdoctorat. 
Il s’adresse à la fois aux *étudiants canadiens* et aux *étudiants internationaux*. 

Ce guide est divisé en **quatre chapitres**. 
Ces chapitres ont été rédigés par les représentants du centre (le responsable de chaque partie est indiqué entre parenthèses).

1. L’Université de Montréal (ancien guide + G.T. Vallet)
2. Le CRIUGM (G.T. Vallet)
3. La vie quotidienne (M. Montembeault)
4. Volet international (B. Houzé)

*La première partie est consacrée à l‘Université de Montréal*.
En effet, le centre et une majorité de ses chercheurs sont affiliés à l’Université de Montréal. 
Pour cette raison, la plupart des étudiants du centre sont également inscrits dans cette université.
Ainsi, ce guide présente les principales démarches à accomplir vis-à-vis de l’Université en tant qu’étudiant. 
Nous présentons également quelques ressources utiles offertes par l’université.

Dans le cas où vous dépendriez d’une autre université, des démarches similaires à celles décrites pour l'UdeM seront probablement à effectuer auprès de votre université d'affiliation. 
Nous vous invitons alors à vous renseigner auprès des autres universités directement (UQÀM, McGill, Université de Concordia, etc.).

*La seconde partie est dédiée au centre lui-même* avec sa présentation et son fonctionnement.
Ce chapitre vous situera le centre d’un point de vue historique et administratif, et surtout nous vous détaillerons les démarches à suivre et les services qui y sont offerts.

*La troisième partie concerne la vie quotidienne à Montréal*. 
Vous y trouverez des adresses utiles et des conseils pour les personnes qui viennent d’arriver sur Montréal ou qui cherchent à s’orienter davantage dans le quartier de Côte-des-Neiges où se situe le centre.

Finalement, *la quatrième partie présente les démarches d’immigration* et concerne donc les étudiants étrangers.
Vous y trouverez des liens et informations utiles pour vous repérer dans vos démarches pour venir au centre. 
**Attention**, ces informations ne se substituent aucunement aux indications de l’immigration canadienne. 
Il est donc capital que vous preniez des renseignements directement auprès des agences gouvernementales concernées.

Pour toutes questions ou informations supplémentaires, merci de contacter les représentants étudiants du centre de recherche ([rep.etudiants.criugm@gmail.com](mailto:rep.etudiants.criugm@gmail.com)) ou la direction administrative du centre (Mme Paulette Espinosa [paulette.espinosa@criugm.qc.ca](mailto:paulette.espinosa@criugm.qc.ca)).


\vspace{.3cm}
**Remerciements**

Les auteurs souhaitent remercier sincérement Édith Durand et Johane Landry pour leurs corrections, ainsi que la direction du CRIUGM pour leur soutien.


\vspace{.3cm}
**Licence et droit d’utilisation**

*Le contenu de ce guide est protégé par la licence Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. Vous pouvez consulter cette licence à cette adresse [http://creativecommons.org/licenses/by-nc-sa/4.0/](http://creativecommons.org/licenses/by-nc-sa/4.0/). Vous êtes libre d’utiliser son contenu, de le modifier et de le partager à nouveau tant que vous respectez les conditions énoncées dans la licence. Aucune utilisation commerciale de ce guide n’est permise.*

![](by-nc-sa.png)

