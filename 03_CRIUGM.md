---
title: CRIGUM
author: GT Vallet
---

# Centre de Recherche de l’IUGM

## Informations 

### IUGM 
L'IUGM est un centre de soins spécialisés pour les personnes âgées créé en 1978 et connu alors sous le nom de *centre hospitalier Côte-des-Neiges*. 
Le centre de recherche lui-même sera crée en 1982 et l’affiliation à l’Université de Montréal sera effective un an plus tard en 1983. 
Ainsi, le centre deviendra l’*Institut Universitaire de Gériatrie de Montréal* en 1997. 
L’institut possède une capacité d’accueil de 452 lits et offre différents programmes de courte et de longue durée dont voici la liste :
réadaptation fonctionnelle intensive;
évaluation de courte durée gériatrique;
hébergement et soins de longue durée;
centre ambulatoire comprenant des activités de centre de jour, d'hôpital de jour et des cliniques externes spécialisées (clinique de cognition, clinique de dysphagie, clinique de continence urinaire et clinique de gestion de la douleur chronique).

L'IUGM compte près de 1 000 employés regroupant le corps médical et non médical.

### CRIUGM
Le centre de recherche regroupe une cinquantaine de chercheurs, une trentaine de postdoctorants et près de trois cents étudiants de second et troisième cycle. 

L’activité de recherche du centre s’organise autour de deux axes. 
Le premier axe est dédié aux neurosciences du vieillissement (environ 25 chercheurs) et le second axe à la promotion de la santé, soins et intervention (environ 25 chercheurs également).

Le centre de recherche dispose également de son propre *comité d’éthique de la recherche* ainsi qu’une *unité de neuroimagerie fonctionnelle* (UNF) équipée d’un IRM Siemens 3 Tesla ainsi que d’un appareil d’imagerie neuronale optique (CW5).  

## La vie du centre au quotidien 

### Stationnement
La vie quotidienne au centre de recherche est très liée aux services disponibles au centre et à l’institut. 
L’accès en voiture au centre se fait facilement via les principaux axes routiers environnant.
Il est alors possible de garer sa voiture sur le stationnement du centre de recherche situé devant son entrée au 4545 chemin Queen Mary. 
Un billet de stationnement pour *la journée* coûte environ 4$ et il est possible d’obtenir une *vignette pour un mois* complet au prix de 52$ (prix en 2016).
Ces billets et vignettes sont disponibles à l’accueil du **service de sécurité au 2\up{e} étage de l’IUGM** (entrée 4565 chemin Queen Mary). 

Il est également possible de se stationner gratuitement dans les rues aux alentours du centre notamment les rues *Cedar Crescent*, *Roslyn*, *Stanley Weir*, etc. 
Cependant, la place n’est alors pas garantie (voir aussi la section 3.2 de ce guide).

**Informations recherche:** Il est à noter que des billets de stationnement pour les participants aux projets de recherche sont disponibles au prix de 4$ (prix en 2016).
Ces billets sont disponibles à la sécurité, mais aussi auprès de Régine Paul (bureau M7812 au 7\up{e} étage). 
Pour les démarches à suivre et l’usage de ces billets, nous vous conseillons d’en parler avec votre superviseur.

### Repas et café
Le centre met à disposition de ses membres des **réfrigérateurs** et des **micro-ondes** situés au 5\up{e} (salle dédiée à gauche en sortant de l’ascenseur) et 6\up{e} étage (Salle d’Organisation Logistique - SOL) . 
Le **salon du 30\up{e} anniversaire** du CRIUGM situé au 5\up{e} peut servir de coin de repas avec ses différentes tables et chaises disponibles. 
Sinon, les membres du centre peuvent accéder à **la cafétéria de l’IUGM** situé au rez-de-chaussée de l’IUGM. 
Des plats chauds ou froids (sandwichs, bar à salades) y sont *servis entre 11 h 30 et 13 h 30* tous les midis et tous les soirs.
Le **Café Perce-Neige** situé en face de la sécurité au 2\up{e} étage de l’IUGM propose également quelques plats (bagels, sandwichs) en plus de confiseries, muffins, café et autres boissons.


## Badge, clés et garderie


### Badge

Les nouveaux membres du centre de centre doivent établir une carte d’identité (badge) afin de faciliter leur identification dans le centre (notamment auprès de la sécurité). 
Avant toute chose, vous devrez rencontrer **Johane Landry** qui s’occupe de l’accueil des nouveaux étudiants. 
Lors de cette rencontre, Johane vous fournira de nombreuses informations utiles.
C’est également à cette occasion que Johane recueillera quelques renseignements nécessaires pour établir votre carte d’identité.

Il faudra ensuite remplir quelques formalités administratives auprès de **Paulette Espinosa** (bureau M7812 au 7\up{e} étage). 
Une fois celles-ci complétées, vous devrez vous rendre à l’administration au 2\up{e} étage auprès de **Joanne Clermont** (bureau M2824) pour prendre une photographie et lancer la préparation du badge.
Celui-ci sera disponible dans les semaines suivantes.

### Clés
Des clés vous seront nécessaires pour l’accès à votre bureau et aux laboratoires de votre équipe comme les salle d’expérimentations.
Chaque laboratoire peut avoir sa propre politique de gestion des clés des salles communes (à discuter avec votre superviseur).
D’une manière générale, la demande de clés se fait auprès d’Émilie Dessureault (bureau M7808, 7\up{e} étage, [guideti@criugm.qc.ca](mailto:guideti@criugm.qc.ca)).
Une fois la demande envoyée et validée par le service de sécurité de l’IUGM, vous pourrez retirer votre clé au comptoir d’accueil de la sécurité au 2\up{e} étage de l’IUGM. 
Une caution de 25$ vous sera demandée et vous sera retournée lorsque vous rendrez la ou les clés dont vous avez la charge.

### Garderie
L’IUGM met à disposition de ses employés une garderie (i.e. crèche pour les français). 
Il vous faudra donc travailler pour le centre (postdoctorant, professionnel de recherche, etc.) pour pouvoir faire une demande d’accès. 
Il est à noter d’ailleurs que le nombre de places disponibles est limité.
Le prix varie selon votre statut et vos revenus. 

Sinon, il existe des garderies accessibles dans chacun des quartiers de la ville, certaines sont publiques, d’autres privées. 
Au Québec, les garderies sont encadrées par la loi, nous vous recommandons donc de bien vous renseigner à l’avance, par exemple sur Internet [http://www.magarderie.com/](http://www.magarderie.com/).
Il est à noter que les garderies publiques sont au prix de 7$ par jour et par enfant (prix en 2016).


## Services informatiques

Le centre dispose d’un service informatique qui offre un service de maintenance ainsi que des facilités techniques.
Vous pouvez contacter le service informatique soit en vous rendant à leur bureau (RS727) soit en contactant ses membres :

- *Johane Landry* : poste 3369, [johane.landry@criugm.qc.ca](mailto:johane.landry@criugm.qc.ca)
- *Derek Yu* : poste 3364, [derek.yu@criugm.qc.ca](mailto:derek.yu@criugm.qc.ca)
- *Dominic Beaulieu* : poste 4070, [dominic.beaulieu@criugm.qc.ca](mailto:dominic.beaulieu@criugm.qc.ca)

Parmi les services proposés, les plus notables sont les suivants.

### Adresse courriel
Le centre propose à ses employés d’obtenir leur propre adresse courriel au centre (`votre_nom@criugm.qc.ca`).
Ce service vise plus particulièrement les employés et les postdoctorants.
Il est en effet moins utile pour les étudiants au second et troisième cycle qui possèdent déjà une adresse courriel auprès de leur université. 

La création d’une adresse courriel s’accompagne de l’accès au webmail du centre (site internet pour gérer ses messages, comme Gmail.com ou Outlook.com).

### Serveurs du centre
Le centre offre également l’accès à un **gestionnaire de fichiers** hébergé sur les serveurs du centre (basé sur Seafile : [http://www.seafile.com/](http://www.seafile.com/en/home/)). 
Ce service est très utile puisqu’il vous permettra de sauvegarder vos données de recherche, et également de pouvoir accéder aux différentes versions de vos fichiers sauvegardés (historique des changements au fil du temps).

### Imprimantes
En tant que membre du centre vous avez également accès aux imprimantes du centre.
Il existe une imprimante monochrome multiservices à chaque étage et une imprimante couleur au sous-sol du centre. 
L’accès à l’imprimante couleur est restreint (voir avec votre supersiveur), et l’accès aux imprimantes monochromes se fait suite à la configuration de votre ordinateur par le service informatique.



